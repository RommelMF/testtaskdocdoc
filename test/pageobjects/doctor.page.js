const Page = require('./page');


class DoctorPage extends Page {
    
    get doctorCards () { return $$('[data-test-id="doctor-card-search-results"]') }
    get buttonCalendar () { return $('[data-test-id="calendar-button"]') }
    get titleButtonCalendar () { return $('[data-test-id="calendar-button"] [class="select-box__title"]') }
    get elementsCalendar () { return $$('[data-test-id*="calendar-item."]') }
    get elemAllDays () { return $('[data-test-id="date_select"]') }
    get elemWithAction () { return $('[data-test-id="date_select"] [class="select-box__options-item-active-icon"]') }
    get elemOnTomorrow () { return $('[data-test-id="calendar-item.1"]') }
    get elemLinkNextSchedule () { return $('[class="clinic-slots__caption-link-date"]') }

    
    openCalendar () {
        this.buttonCalendar.click();
    }

    selectCalendarOnTomorrow() {
        this.elemOnTomorrow.click();
    }

    open () {
        return super.open('doctor');
    }
}

module.exports = new DoctorPage();
