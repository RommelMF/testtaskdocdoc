var should = require('should');
var doctorPage = require('../pageobjects/doctor.page')


describe('Test page doctor', function(){

    before(function(){
        doctorPage.open();
    });

    it('Should view 10 doctor cards', function(done){
        should(doctorPage.doctorCards.length).be.exactly(10);
    });
    it('Should view calendar button', function(done){
        should.exist(doctorPage.buttonCalendar);
    });
    it('Should calendar button get text', function(done){
        should(doctorPage.titleButtonCalendar.getText()).be.exactly('Расписание на все дни');
    });
    it('Should list values for dates', function(done){
        doctorPage.openCalendar();
        should.exist(doctorPage.elementsCalendar);
    });
    it('Should view action-icon on elem', function(done){
        should(doctorPage.elemAllDays.getText()).be.exactly('Все дни');
        should.exist(doctorPage.elemWithAction);
    });
    it('Should button has text', function(done){
        doctorPage.selectCalendarOnTomorrow();
        should(doctorPage.titleButtonCalendar.getText()).be.exactly('Расписание на завтра');
    });
    it('Should view 10 doctor cards again', function(done){
        should(doctorPage.doctorCards.length).be.exactly(10);
    });
    it('Should doctors are active', function(done){
        //Если отображается этот элемент, то у доктора не отображается актуальное расписание на текущий день
        doctorPage.elemLinkNextSchedule.should.not.exist;
    });

});